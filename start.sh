#!/usr/bin/env bash
python3 ./sqlScripts/initiate.py
go build -o ./app/goapp ./app/go_app.go
go build -o ./webAPI/goapi ./webAPI/main_api.go
./app/goapp &
./webAPI/goapi &