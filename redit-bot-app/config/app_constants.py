# Created by Nandagopal
# 14/02/2021

"""Reads and return constant values for the DB connectors.

"""

import yaml
import os.path


class AppConstants():

	_instance = None

	"""Check whether the instance for the class is already available
	if the instance is available then the instance will be returned
	else a new instance will be created
	"""

	def __new__(cls):
		if AppConstants._instance is None:
			AppConstants._instance = object.__new__(cls)
			AppConstants._instance.__conf_file = "/configuration.yaml"
			AppConstants._instance.__conf_file_path = os.path.dirname(os.path.realpath(__file__)) + AppConstants._instance.__conf_file
		return AppConstants._instance

	def __init__(self):
		self.__conf_file_path = AppConstants._instance.__conf_file_path
		self.__conf_data = {}
		with open(self.__conf_file_path, 'r') as yaml_data:
			config_data = yaml.safe_load(yaml_data)
			self.__conf_data.update(config_data)

	# getters for all constants from the config file

	@property
	def DB_DIALECT(self):
		return self.__conf_data['MySQL']['DB_DIALECT']

	@property
	def DB_USER(self):
		return self.__conf_data['MySQL']['DB_USER']

	@property
	def DB_PASS(self):
		return self.__conf_data['MySQL']['DB_PASS']

	@property
	def DB_IP(self):
		return self.__conf_data['MySQL']['DB_IP']

	@property
	def DB_PORT(self):
		return self.__conf_data['MySQL']['DB_PORT']

	@property
	def DB(self):
		return self.__conf_data['MySQL']['DB']