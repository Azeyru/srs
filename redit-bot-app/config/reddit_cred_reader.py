# Created by Nandagopal
# 14/02/2021

"""Reads and return constant values for Reddit Auth.

"""

import yaml
import os.path
import praw


class RedditCreds():

	def __init__(self):
		self.__conf_data = {}
		__conf_file = "/creds.yaml"
		self.__conf_file_path = os.path.dirname(os.path.realpath(__file__)) + __conf_file

		with open(self.__conf_file_path, 'r') as yaml_data:
			config_data = yaml.safe_load(yaml_data)
			self.__conf_data.update(config_data)

	@property
	def USER_AGENT(self):
		return self.__conf_data['REDDIT.CRED']['USER_AGENT']

	@property
	def CLIENT_ID(self):
		return self.__conf_data['REDDIT.CRED']['CLIENT_ID']

	@property
	def CLIENT_SECRET(self):
		return self.__conf_data['REDDIT.CRED']['CLIENT_SECRET']

	# Returns Reddit Auth
	def REDDIT_AUTH(self):
		reddit_obj = RedditCreds()
		self.reddit_cred_return = praw.Reddit(user_agent=reddit_obj.USER_AGENT,
                                    client_id=reddit_obj.CLIENT_ID,
                                    client_secret=reddit_obj.CLIENT_SECRET
                                    )
		return self.reddit_cred_return