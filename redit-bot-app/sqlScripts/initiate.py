# Created by Nandagopal
# 25-02-2021

""" Checks for database schema and tables, 
if not available will be run inside the container.

"""

import mysql.connector
from dotenv import load_dotenv
import os


class InitiateDBCheck():

	# Returns the instance of the connection if already initiated
	def __init__(self):
		load_dotenv()
		self.connection = mysql.connector.connect(host="localhost",
                                             ssl_disabled=False,
                                             user="root",
                                             passwd="root")

		self.create_database()
		self.create_tables()

	# Create a new schema if not available 
	def create_database(self):

		try:
			print(f"Creating new Schema if not available")

			cursor = self.connection.cursor()
			db = "reddit_bot"
			cursor.execute(f"CREATE DATABASE {db}")
			
			print(f'Created a new schema')
			
			cursor.close()

		except Exception as e:
			print(f'Table already available: {e}')


	# Create tables 
	def create_tables(self):

		print(f"Checking for storage...")

		cursor = self.connection.cursor()

		# Scans for .sql file and executes the availabe scirpt in the file
		cwd = os.path.dirname(os.path.realpath(__file__))
		with open(cwd + "/schema.sql") as sql_scripts:

			for sql_script in sql_scripts:
				try:
					print(f'Running Script {sql_script}')
					cursor.execute(str(sql_script))
					print(f'Executed successfully ')

				except Exception as e:
					print(f'Table Available')

		cursor.close()


# Initiate the class
if __name__ == "__main__":
	InitiateDBCheck()
