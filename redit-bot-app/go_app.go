// Created by Nandagopal
// Date: 15/02/2021

package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"

	"bytes"
	"time"

	"nandagopal/reddit-bot-app/config"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	queueProcess()
}

func queueProcess() {
	fmt.Println("starting job queue processer")

	conf := config.GetAppConfig()
	hours, _ := strconv.Atoi(conf.GetHR())
	second, _ := strconv.Atoi(conf.GetSEC())

	for {
		runRedditBotCore()
		fmt.Println("Updated Data with latest Subreddit and scores")
		time.Sleep(time.Duration(hours) * time.Duration(second) * time.Second)
		ClearFlags()
	}
}

func runRedditBotCore() {

	fmt.Println("Initiating Python reddit scraper")

	cmd := exec.Command("python3", "-m", "luigi", "--module", "runner", "MainRunner", "--local-scheduler")
	time.Sleep(10 * time.Second)
	err := cmd.Run()

	var out bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &out
	cmd.Stderr = &stderr

	if err != nil {
		fmt.Println("Error while executing the Scripts")
		fmt.Println(err, stderr.String())
	} else {
		fmt.Println("Completed Succesfully")
		fmt.Println(err, out.String())
	}
}

func removeFiles(filename string) {

	common_folder, _ := os.Getwd()
	cf := common_folder + "/reddit_score_calc/"

	err := os.Remove(cf + filename)
	if err != nil {
		fmt.Println("Error while removing the file:", err.Error())
	}
}

func ClearFlags() {

	fmt.Println("Removing flag files")
	file_list := []string{"redditlist.txt", "postcompletion.txt", "commentcompletion.txt"}

	for i := 0; i < len(file_list); i++ {
		removeFiles(file_list[i])
	}
}
