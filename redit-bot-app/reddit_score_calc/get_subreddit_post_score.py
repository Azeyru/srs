# Created by Nandagopal
# 14/02/2021

"""Gets and stores the ID of all N Posts for all the subreddit list in DB,
entry depending on the unavailability of post's if in dd,The number of 
posts are read from the preconditons/.env.
"""

import os
import luigi

from config.reddit_cred_reader import RedditCreds
from db.db_connector import GetDBConnector


class GetPostScore(luigi.Task):

	max_posts = luigi.IntParameter(default=os.getenv("SUBREDDIT_POSTS"))

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		auth_obj = RedditCreds()
		self.reddit_auth = auth_obj.REDDIT_AUTH()

	def run(self):

		connection = GetDBConnector().get_new_connection()
		cursor = connection.cursor()

		try:
			cursor.execute("SELECT id, sub_name FROM reddit_bot.subreddit_table;")
			# Contains list of all Subreddits in DB.
			subreddit_list = cursor.fetchall()

			cursor.execute("SELECT title_id FROM reddit_bot.post_table;")
			# Containsd list of all the Posts available in DB.
			title_id_list = cursor.fetchall()

			connection.commit()

		except Exception as e:
			print(e)

		for ids, subreddit in subreddit_list:
			top_posts = self.reddit_auth.subreddit(subreddit).hot(limit=self.max_posts)
			for posts in top_posts:
				# New Post ID will along with Subreddit ID will be added to DB.
				if not posts.id in str(title_id_list):
					try:
						cursor.execute("INSERT INTO reddit_bot.post_table (sub_id, title_id) VALUES (%s, %s);",[ids, posts.id])
						connection.commit()

					except Exception as e:
						print(e)

		# File as flag for GetPostScore completion
		cwd_flag = os.path.dirname(os.path.realpath(__file__)) + "/postcompletion.txt"
		with open(cwd_flag, "w") as flag:
			flag.write("Just a flag")

	# Checks whether the GetPostScore is already completed based on the Flag
	def output(self):
		return luigi.LocalTarget(os.path.dirname(os.path.realpath(__file__)) + "/postcompletion.txt")

