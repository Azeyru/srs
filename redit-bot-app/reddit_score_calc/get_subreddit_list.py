# Created by Nandagopal
# 14/02/2021

""" Reddit API will be called using a general subreddit where multiple 
reddit posts are displayed. The method will call the set number of subreddits 
which will be then entered to the database depending on whether the subreddit 
name is already available in the DB or not.
"""

import os
import luigi

from config.reddit_cred_reader import RedditCreds
from db.db_connector import GetDBConnector


class GetSubredditList(luigi.Task):

	top_n_subreddits = luigi.IntParameter(default=os.getenv("SUBREDDIT_LIMIT"))

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		auth_obj = RedditCreds()
		self.reddit_auth = auth_obj.REDDIT_AUTH()

	# Process will update the DB and create a file as flag for compeltion.
	def run(self):
		top_subreddit = self.reddit_auth.subreddit("all").hot(limit=self.top_n_subreddits)

		connection = GetDBConnector().get_new_connection()
		cursor = connection.cursor()
		try:
			cursor.execute("SELECT sub_name FROM reddit_bot.subreddit_table;")
			# Contains the list of subreddits available in DB.
			asset_info = cursor.fetchall()
			connection.commit()

		except Exception as e:
			print(e)

		try:
			cwd = os.path.dirname(os.path.realpath(__file__))
			with open(cwd+"/redditlist.txt", "w") as file:
				for submission in top_subreddit:
					file.writelines(f"{submission.subreddit}: {submission.id}" + '\n')
					# If Subreddit is not available in DB, it will be appended.
					if not str(submission.subreddit) in str(asset_info):
						cursor.execute("INSERT INTO reddit_bot.subreddit_table (sub_name) VALUES (%s) ;",[str(submission.subreddit)])
						connection.commit()

		except Exception as e:
			print(e)

		finally:
			cursor.close()

	# Checks whether the flag for GetSubredditList is avaiable or not.
	def output(self):
		return luigi.LocalTarget(os.path.dirname(os.path.realpath(__file__)) + "/redditlist.txt")

