# Created by Nandagopal
# 14/02/2021

"""Get Comment score, ID and store to DB along with post ID.
If duplicates are present the Score will be updated, else new
records will be added to the fields.
"""

import os
import luigi

from config.reddit_cred_reader import RedditCreds
from db.db_connector import GetDBConnector
from reddit_score_calc.get_subreddit_post_score import GetPostScore


class GetCommentScore(luigi.Task):

	max_posts = luigi.IntParameter(default=os.getenv("SUBREDDIT_POSTS"))
	max_comments = luigi.IntParameter(default=os.getenv("POST_COMMNETS"))

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		auth_obj = RedditCreds()
		self.reddit_auth = auth_obj.REDDIT_AUTH()

	# Calls GetPostScore as a pipeline prerequisite.
	def requires(self):
		return GetPostScore(max_posts=self.max_posts)

	def run(self):

		connection = GetDBConnector().get_new_connection()
		cursor = connection.cursor()

		try:
			cursor.execute("SELECT comment_id FROM reddit_bot.com_table;")
			# Contains list of existing comments_id in DB.
			comments_list = cursor.fetchall()

			cursor.execute("SELECT title_id FROM reddit_bot.post_table;")
			# Contains list of existing posts_id in DB.
			posts_list = cursor.fetchall()

			connection.commit()

		except Exception as e:
			print(e)


		for post in posts_list:
			top_n_posts = self.reddit_auth.submission(post[0])
			top_n_posts.comments.replace_more(limit=0)

			comment_count = 0
			for comment in top_n_posts.comments:
				if not comment.id in str(comments_list):
					# Inserts the comment ID and Score to the fields if not already present.
					try:
						cursor.execute("INSERT INTO reddit_bot.com_table (post_id, comment_id, score) VALUES (%s, %s, %s);",[post[0], comment.id, comment.score])
						connection.commit()
					except Exception as e:
						print(e)

				else:
					# Updates the Score field with the latest score if comment ID already present in DB.
					try:
						cursor.execute("UPDATE reddit_bot.com_table SET score = %s WHERE comment_id = %s;",[comment.score, comment.id])
						connection.commit()
					except Exception as e:
						print(e)
				comment_count = comment_count + 1
				# Checks whether comments/score are called only for max_comments.
				if comment_count == self.max_comments:
					break

		# File as flag for GetCommentScore completion
		cwd_flag = os.path.dirname(os.path.realpath(__file__)) + "/commentcompletion.txt"
		with open(cwd_flag, "w") as flag:
			flag.write("Just a flag for comments")

	# Checks whether the GetCommentScore is already completed based on the Flag
	def output(self):
		return luigi.LocalTarget(os.path.dirname(os.path.realpath(__file__)) + "/commentcompletion.txt")