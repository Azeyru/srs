# Created by Azeyru
# 14/02/2021

"""Objective is to get top trending subreddits, 
depeding on the Score of top N commnets under M Post for each subreddits
Primary pipeline task: MainRunner
The data will be entered to a DB, and can be used for future reference and analysis.
"""

import os
import luigi
import time

from dotenv import load_dotenv
from config.app_constants import AppConstants
from reddit_score_calc.get_subreddit_list import GetSubredditList
from reddit_score_calc.get_subreddit_comment_score import GetCommentScore


class MainRunner(luigi.Task):

	schedule_compelte = False
	start_time = time.time()
	load_dotenv()

	top_n_subreddits = luigi.IntParameter(default=int(os.getenv("SUBREDDIT_LIMIT")))
	max_posts = luigi.IntParameter(default=int(os.getenv("SUBREDDIT_POSTS")))
	max_comments = luigi.IntParameter(default=int(os.getenv("POST_COMMNETS")))

	# Initiates GetSubredditList followed by GetCommentScore. Scores and IDs will be populated in DB.
	def requires(self):
		return [
				GetSubredditList(top_n_subreddits=self.top_n_subreddits), 
				GetCommentScore(max_posts=self.max_posts, max_comments=self.max_comments)
			]

	def run(self):
		time_taken = time.time() - MainRunner.start_time
		print(f"Time Taken: {time_taken}")
		self.schedule_compelte = True

	def complete(self):
		return self.schedule_compelte


#if __name__ == '__main__':
	#luigi.build([MainRunner(top_n_subreddits=10, max_posts=5, max_comments=2)], local_scheduler=True)
	#luigi.build([MainRunner()], local_scheduler=True)