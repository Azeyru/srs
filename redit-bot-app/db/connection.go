// Created by Nandagopal
// Date: 15/02/2021

// Returns connection instance

package query

import (
	"database/sql"
	"fmt"

	"nandagopal/reddit-bot-app/config"

	_ "github.com/go-sql-driver/mysql"
)

func InitilizeDB() *sql.DB {
	var err error
	var connection *sql.DB
	config := config.GetAppConfig()
	remote := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=UTC",
		config.GetDBUser(),
		config.GetDBPass(),
		config.GetDBIp(),
		config.GetDBPort(),
		config.GetDB())
	if connection, err = sql.Open(config.GetDialect(), remote); err != nil {
		fmt.Println("Failed Connecting to Database")
		panic(err.Error())
	}
	fmt.Printf("Connected to %s database[%s]\n", config.GetDialect(), config.GetDB())
	return connection
}
