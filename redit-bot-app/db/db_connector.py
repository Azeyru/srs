# Created by Nandagopal
# 14/02/2021

"""
Method for DB connection
"""

import mysql.connector
import os

from config.app_constants import AppConstants


class GetDBConnector():

	def __init__(self):
		self._DB = None


	# Returns the instance of the connection if already initiated
	def get_new_connection(self):
		app_constant = AppConstants()
		print(20*"--")
		print(app_constant.DB_IP)
		connection = mysql.connector.connect(host=app_constant.DB_IP,
                                             ssl_disabled=False ,
                                             user=app_constant.DB_USER,
                                             passwd=app_constant.DB_PASS,
                                             database=app_constant.DB)
		return connection

	# Returns the instance on call
	def __enter__(self):
		self._DB = self.get_new_connection()
		return self._DB

	# Closes the instances on exit
	def __exit__(self):
		self._DB.close()