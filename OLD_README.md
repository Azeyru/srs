# Table of contents
-------------------
 * Introduction
 * Requirements
 * Prerequisites
 * Installation
 * Configurations
 * Database schema
 * Release Note
 * Scope for improvement
 * File Organization

## Introduction
---------------
The App provides updates list of top N subreddit. The number of subreddit to be displayed is configurable.
A Web API must be hit to retrieve the updated List. The app once initiated, will refresh the Scores once every 12 hours.
All attributes of the APP can be configured my editing **configurations.yaml and .env files**

**Technologies (Requirements)**
-------------------------------
1. Python 3.7+
2. Go
3. MySQL

**Prerequisites/Modules (To be executed in bash/cmd)**
------------------------------------------------------
### Python Packages
	1. pip install pyyaml
	2. pip install luigi
	3. pip install python-dotenv
	4. pip install mysql-connector-python

### Go Packages
	1. go get github.com/joho/godotenv
	2. go get github.com/gorilla/mux
	3. go get github.com/go-sql-driver/mysql
	4. go get gopkg.in/yaml.v2

**Installation/Development Environment Setup**
----------------------------------------------
1. Extract the package to a folder
2. Execute the Queries under **/sqlScripts/schema.sql** for MySQL.
3. Navigate to **/app** and run go_app using the command: **go run go_app.go** via bash.
	[This is start Python scripts which will start fetching data using Reddit API and pushed to MySQL DB]
4. Navigate to **/webAPI** and run main_api using **go run main_api.go**
5. Go to **http://localhost:9776/top50** to get the top 50 subreddit list

**Configurations** (Availabe for both APP(.env) and API(configs/configuration.yaml))
------------------
*SUBREDDIT_LIMIT*  : Number of Subreddit scores to be updates and displayed
*SUBREDDIT_POSTS*  : Max number of post to be used to get the Subreddit score
*POST_COMMNETS*    : Number of comments used for getting Post score.
*MULTIPLE_OF_HOUR* : Hours to wait for next schedule
*IN_SECONDS*       : Hours in seconds
          Sample   : 12 Hours = 12(MULTIPLE_OF_HOUR) x 3600(IN_SECONDS) Seconds

### Database schema
-------------------
    1. subreddit_table
        * id: Holds Subreddit ID based on insertion
        * sub_name: Subreddit Title
    2. post_table
        * id: Post insertion ID
        * sub_id: Subreddit ID
        * title_id: Post Title ID from API 
    3. com_table
        * id: Comment insertion ID
        * post_id: Post Title ID from API 
        * comment_id: Comment ID from API
        * score: Score of individual comment 

**Release Note**
----------------
 Release version 0.1
    Added: Functions to retreive data from reddit API, store them and display the Top N subreddit using a Web API.

**Scope for Improvement**
-------------------------
~~~
1. Docker can be introduced to add containerization and better modularity.
2. Installation process can be automated.
3. Better design process can be introduced.
4. Model to implement day to day difference in Subreddit scores can be introduced for Analytics.
5. Unit Tests can be introduced.
~~~

### File Organization
---------------------
~~~
├── app                     -> Contains Python Scraper and Go Runner
│  ├─── config              -> Contains configs for Go and Python scripts
│  ├─── db                  -> DB conncetion methods
│  ├─── creddit_score_calc  -> Scraper Logic
│  ├─── .env                -> Variables for getting Subreddit List
│  ├─── go_app.go           -> Used for initiaitng and scheduling Reddit Scraper
│  ├─── run.sh              -> Shell script for starting Luigi
│  └─── runner.py           -> Luigi Task
│
├── sqlScripts
│   └── schema.sql          -> Contains SQL Scripts
│
├── webAPI                  -> Go RESTAPI for getting Subreddit List
│   ├─── configs            -> Configs for GO API
│   ├─── controller         -> Retreving Subreddit list as Json response 
│   ├─── db                 -> DB conncetion method
│   ├─── repo               -> Methods for executing queries for N subreddit list
│   ├─── routers            -> RESTapi blueprints
│   ├─── utils              -> helper methods
│   └─── main_api.go        -> main runner for Go RESTapi service
│
├── requirements.txt        -> list of all GO and Python Modules for getting the apps up and ruinning
│
└── README.md               -> README for Developers
~~~
