# Table of contents
-------------------
 * Introduction
 * Technologies
 * Requirements
 * Modules Used
 * Installation
 * Getting Results
 * Configurations
 * Database schema
 * Release Note
 * Scope for improvement
 * File Organization

## Introduction
---------------
The App provides updates list of top N subreddit. The number of subreddit to be displayed is configurable.
A Web API must be hit to retrieve the updated List. The app once initiated, will refresh the Scores once every 12 hours.
All attributes of the APP can be configured by editing **configurations.yaml and .env files**

**Requirements**
----------------
1. Latest Docker

**Technologies**
----------------
1. Python 3.7+
2. Go
3. MySQL
4. Docker

**Modules Used**
----------------
### Python Packages
	1. pyyaml
	2. luigi
	3. python-dotenv
	4. mysql-connector-python
    5. praw

### Go Packages
	1. godotenv
	2. mux
	3. mysql
	4. yaml.v2

**Installation/Development Environment Setup**
----------------------------------------------
1. Extract the package to a folder
2. Execute the shell script **setup.sh**: This will start build for Docker containers for all the apps (Auto exits).
3. Execute **reddit.sh**: All the services in containers will be initiated, the reddit api bot will 
   start to pull data (could take couple of minutes for scraping).

**Getting Results**
-------------------
1. Open power shell from the app root(Where READMD is placed)
2. Execute **docker exec -it reddit_bot_containerized_api_1 bash** (containers must be up and running)
3. Enter **curl localhost:9776/top50**

**Configurations** (Availabe for both APP(.env) and API(configs/configuration.yaml))
------------------
*SUBREDDIT_LIMIT*  : Number of Subreddit scores to be updates and displayed
*SUBREDDIT_POSTS*  : Max number of post to be used to get the Subreddit score
*POST_COMMNETS*    : Number of comments used for getting Post score.
*MULTIPLE_OF_HOUR* : Hours to wait for next schedule
*IN_SECONDS*       : Hours in seconds
          Sample   : 12 Hours = 12(MULTIPLE_OF_HOUR) x 3600(IN_SECONDS) Seconds

### Database schema
-------------------
    1. subreddit_table
        * id: Holds Subreddit ID based on insertion
        * sub_name: Subreddit Title
    2. post_table
        * id: Post insertion ID
        * sub_id: Subreddit ID
        * title_id: Post Title ID from API 
    3. com_table
        * id: Comment insertion ID
        * post_id: Post Title ID from API 
        * comment_id: Comment ID from API
        * score: Score of individual comment 

**Release Note**
----------------
 Release version 0.1
    Added: Functions to retreive data from reddit API, store them and display the Top N subreddit using a Web API.
 Release version 0.2
    Added: Docker has been introduced.
    The apps has been restructured to adapt to the container environment.

**Scope for Improvement**
-------------------------
~~~
1. D̶o̶c̶k̶e̶r̶ ̶c̶a̶n̶ ̶b̶e̶ ̶i̶n̶t̶r̶o̶d̶u̶c̶e̶d̶ ̶t̶o̶ ̶a̶d̶d̶ ̶c̶o̶n̶t̶a̶i̶n̶e̶r̶i̶z̶a̶t̶i̶o̶n̶ ̶a̶n̶d̶ ̶b̶e̶t̶t̶e̶r̶ ̶m̶o̶d̶u̶l̶a̶r̶i̶t̶y̶.
2. I̶n̶s̶t̶a̶l̶l̶a̶t̶i̶o̶n̶ ̶p̶r̶o̶c̶e̶s̶s̶ ̶c̶a̶n̶ ̶b̶e̶ ̶a̶u̶t̶o̶m̶a̶t̶e̶d̶.
3. Better design process can be introduced.
4. Model to implement day to day difference in Subreddit scores can be introduced for Analytics.
5. Unit Tests can be introduced.
6. HTML could be added.
~~~

### File Organization
---------------------
~~~
Reddit_bot_containerized (root)
│
├── redit-bot-app           -> Contains Python Scraper and Go Runner.
│  ├─── config              -> Contains configs,(**configuration.yaml**) for Go and Python scripts.
│  ├─── db                  -> DB conncetion methods.
│  ├─── reddit_score_calc   -> Scraper Logic.
│  ├─── sqlScripts          -> Contains SQL scripts and python migrator scripts.
│  ├─── .env                -> Variables for getting Subreddit List.
│  ├─── Dockerfile          -> Docker instructions for APP.
│  ├─── go.mod              -> go modules for APP.
│  ├─── go.sum              -> Contains hash of the initial modules used for APP.
│  ├─── go_app.go           -> Used for initiaitng and scheduling Reddit Scraper.
│  ├─── run.sh              -> Shell script for setting up DB.
│  └─── runner.py           -> Luigi Task.
│
├── reddit-bot-api          -> Go RESTAPI for getting Subreddit List
│   ├─── cmd                -> Contains Main runner
│   ├─── internal           -> Retreving Subreddit list as Json response 
│   │       ├─── configs    -> Contains **Configuirations.yaml** for API
│   │       ├─── controller -> REST methods
│   │       ├─── db         -> DB conncetion method
│   │       ├─── repo       -> Methods for executing queries for N subreddit list
│   │       ├─── routers    -> RESTapi blueprints
│   │       └─── utils      -> helper methods
│   ├─── Dockerfile         -> Docker instructions for API.
│   ├─── go.mod             -> go modules for API.
│   └─── go.sum             -> Contains hash of the initial modules used for API.
│
├── docker-compose.yaml     -> list of all GO and Python Modules for getting the apps up and ruinning.
│
├── OLREADME.md             -> Old README for previous release.
│
├── README.md               -> LATEST README for Developers.
│
├── reddit.sh               -> Shell for reddit api scraper
│
└── setup.sh                -> Container Setup Shell
~~~
