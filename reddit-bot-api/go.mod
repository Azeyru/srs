module nandagopal/reddit-bot-api

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	gopkg.in/yaml.v2 v2.4.0
)
