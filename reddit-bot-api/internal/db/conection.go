// Created by Nandagopal
// 16/02/2021

package db

import (
	"database/sql"
	"fmt"
	"nandagopal/reddit-bot-api/internal/configs"

	_ "github.com/go-sql-driver/mysql"
)

// DB connection method
func InitilizeDB() *sql.DB {
	fmt.Println("Connecting to DB...")
	var err error
	var connection *sql.DB

	config := configs.GetAppConfig()
	remote := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=UTC",
		config.GetDBUser(),
		config.GetDBPass(),
		config.GetDBIp(),
		config.GetDBPort(),
		config.GetDB())

	if connection, err = sql.Open(config.GetDialect(), remote); err != nil {
		fmt.Println("Failed Connecting to Database")
		panic(err.Error())
	}

	fmt.Printf("Connected to %s database[%s]\n", config.GetDialect(), config.GetDB())

	return connection
}
