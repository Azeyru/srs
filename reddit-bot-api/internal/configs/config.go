//Created by Nandagopal
//Date: 15/02/2021

package configs

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// yaml custom structure
type mainConfig struct {
	MySQL struct {
		DB_DIALECT string `yaml:"DB_DIALECT"`
		DB_USER    string `yaml:"DB_USER"`
		DB_PASS    string `yaml:"DB_PASS"`
		DB_IP      string `yaml:"DB_IP"`
		DB_PORT    string `yaml:"DB_PORT"`
		DB         string `yaml:"DB"`
	} `yaml:"MySQL"`
	APP struct {
		PORT            string `yaml:"PORT"`
		SUBREDDIT_LIMIT string `yaml:"SUBREDDIT_LIMIT"`
		SUBREDDIT_POSTS string `yaml:"SUBREDDIT_POSTS"`
		POST_COMMNETS   string `yaml:"POST_COMMNETS"`
	} `yaml:"APP"`
}

// yaml reader
func GetAppConfig() mainConfig {
	base_folder, _ := os.Getwd()
	fmt.Println(base_folder)
	yamlFile, err := ioutil.ReadFile(base_folder + "/internal/configs/configuration.yaml")
	if err != nil {
		fmt.Println(err)
		fmt.Println("Error Reading YAML file")
	}

	var conf mainConfig
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil {
		fmt.Println("Error Parsing YAML file")
	}
	return conf
}

func (c *mainConfig) GetDialect() string {
	return c.MySQL.DB_DIALECT
}

func (c *mainConfig) GetDBUser() string {
	return c.MySQL.DB_USER
}

func (c *mainConfig) GetDBPass() string {
	return c.MySQL.DB_PASS
}

func (c *mainConfig) GetDBIp() string {
	return c.MySQL.DB_IP
}

func (c *mainConfig) GetDBPort() string {
	return c.MySQL.DB_PORT
}

func (c *mainConfig) GetDB() string {
	return c.MySQL.DB
}

func (c *mainConfig) GetAppPort() string {
	return c.APP.PORT
}

func (c *mainConfig) GetAppSubs() string {
	return c.APP.SUBREDDIT_LIMIT
}

func (c *mainConfig) GetAppPost() string {
	return c.APP.SUBREDDIT_POSTS
}

func (c *mainConfig) GetAppComments() string {
	return c.APP.POST_COMMNETS
}
