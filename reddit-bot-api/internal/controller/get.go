// Created by Nandagopal
// 16/02/2021

package controller

import (
	"fmt"
	"nandagopal/reddit-bot-api/internal/repo"
	"nandagopal/reddit-bot-api/internal/utils"
	"net/http"
)

// API Get Method 
func APIGetTop(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Getting Top 50 Subreddit")

	top50List := repo.GenreTableRepo()

	w.WriteHeader(http.StatusOK)
	utils.JsonResponse(top50List, w)

}
