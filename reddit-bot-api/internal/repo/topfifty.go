// Created by Nandagopal
// Date: 15/02/2021

package repo

import (
	"fmt"
	"nandagopal/reddit-bot-api/internal/configs"
	"nandagopal/reddit-bot-api/internal/db"
)

// custom return structure
type top50SubReddit struct {
	Sub_name        string //`json:"sub_name"`
	Subreddit_score int    //`json:"subreddit_score"`
}

func GenreTableRepo() []top50SubReddit {
	fmt.Println("Get Top 50")

	top50List := make([]top50SubReddit, 0)
	var sql string

	connection := db.InitilizeDB()
	defer connection.Close()

	config := configs.GetAppConfig()

	sql = `select s1.sub_name, sum(p2.post_score) as subreddit_score from (
			select p1.title_id, p1.sub_id, sum(score) as post_score from (
			select post_id, comment_id, score from reddit_bot.com_table as c2 where (
			select count(*) from reddit_bot.com_table as c1 where c1.post_id = c2.post_id and c1.score >= c2.score) <= %s ) as c3
			inner join reddit_bot.post_table as p1 on p1.title_id = c3.post_id
			group by c3.post_id) as p2
			inner join reddit_bot.subreddit_table s1 on s1.id = p2.sub_id
			group by p2.sub_id order by subreddit_score desc limit %s;`

	sqlInf := []interface{}{
		config.GetAppComments(),
		config.GetAppSubs()}

	rows, err := connection.Query(fmt.Sprintf(sql, sqlInf...))

	if err != nil {
		fmt.Println(err)
	}

	for rows.Next() {
		var top50 top50SubReddit
		if err := rows.Scan(&top50.Sub_name, &top50.Subreddit_score); err == nil {
		}
		top50List = append(top50List, top50)
	}

	rows.Close()

	return top50List

}
