// Created by Nandagopal
// 16/02/2021

package routers

import (
	"fmt"
	"nandagopal/reddit-bot-api/internal/configs"
	"nandagopal/reddit-bot-api/internal/controller"
	"net/http"

	"github.com/gorilla/mux"
)

// Hander method
func HandleRequests() {
	mainRouters := mux.NewRouter().StrictSlash(true)
	mainRouters.HandleFunc("/top50", controller.APIGetTop).Methods("GET")

	mainConf := configs.GetAppConfig()
	port := mainConf.GetAppPort()

	fmt.Println("Listening to " + mainConf.GetAppPort())
	err := http.ListenAndServe(port, mainRouters)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Failed connecting to port " + port)
	}
}
