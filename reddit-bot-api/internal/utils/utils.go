// Created by Nandagopal
// 16/02/2021

package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Json response converter
func JsonResponse(response interface{}, w http.ResponseWriter) {
	fmt.Println("Converting to JSON")

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
}

func ErrorResponse(err error, w http.ResponseWriter) {
	fmt.Println(err)
	w.WriteHeader(http.StatusInternalServerError)
	http.Error(w, "", 501)
	JsonResponse("Failed Loading, Data not found", w)
}
