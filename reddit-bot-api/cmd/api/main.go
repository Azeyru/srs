//Created by Nandagopal
//16/02/2021

package main

import (
	"fmt"
	"nandagopal/reddit-bot-api/internal/routers"
)

// main runner method
func main() {

	fmt.Println("Initiating the Reddit API...")
	routers.HandleRequests()

}
